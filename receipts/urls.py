from django.urls import path, reverse
from receipts.views import (
    receipt_list,
    create_receipt,
    categories,
    accounts,
    create_expense_category,
    create_account,
)
from django.shortcuts import redirect

urlpatterns = [
    path("accounts/create/", create_account, name="create_account"),
    path(
        "categories/create/", create_expense_category, name="create_category"
    ),
    path("accounts/", accounts, name="account_list"),
    path("categories/", categories, name="category_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("", receipt_list, name="home"),
]

from django.urls import reverse
from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, CreateAccountForm


# Create your views here.
@login_required
def receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def categories(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_data = []

    for category in categories:
        receipt_count = Receipt.objects.filter(category=category).count()
        category_data.append(
            {
                "name": category.name,
                "receipt_count": receipt_count,
            }
        )

    context = {
        "category_data": category_data,
    }

    return render(request, "receipts/categories.html", context)


@login_required
def accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    account_data = []

    for account in accounts:
        receipt_count = Receipt.objects.filter(account=account).count()
        account_data.append(
            {
                "name": account.name,
                "number": account.number,
                "receipt_count": receipt_count,
            }
        )

    context = {
        "account_data": account_data,
    }

    return render(request, "receipts/accounts.html", context)


@login_required
def create_expense_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_category.html", context)


def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_account.html", context)
